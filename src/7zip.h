#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers
#include <stdio.h>
#include <tchar.h>

#include <initguid.h>
#include <windows.h>
#include <time.h>

#include "7zip/../Common/Common.h"

// 7-zip version
#include "7zip/MyVersion.h"

#include "Common/StringConvert.h"
#include "7zip/Common/FileStreams.h"
#include "7zip/Archive/7z/7zIn.h"
#include "Windows/FileFind.h"

struct PluginArchiveInfo_t
{
    bool HeadersEncrypted;

    NArchive::N7z::CArchiveVersion Version;
    UInt64 HeadersSize;

    int NumFiles, NumDirs, TotalFiles, NumSolidBlocks;

    bool IsSolid;

    int Ratio;
    UInt64 UncompressedSize;

    char Methods[128];
};

typedef int (*field_handler_proc)(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);

struct SupportedFieldDesc_t
{
    const char* name;
    const char* units;
    int type;
    field_handler_proc handler;
};

extern "C" {

int HeadersEncryptedBooleanHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int HeadersEncryptedStringHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int CompressMethodsHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int NumberOfFilesHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int NumberOfDirsHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int TotalFilesHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int SolidBooleanHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int SolidStringHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int SolidStringHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int NumberOfSolidBlocksHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int HeadersSizeHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int RatioHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int UncompressedSizeHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int UncompressedSizeFormattedHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);
int VersionHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize);

#if (defined(_DEBUG) || defined(__DEBUG__))
static TCHAR _debugStr[255];
#define STRING_AUX(x) #x
#define STRING(x) STRING_AUX(x)
#define DebugString(x, ...)                                                                                  \
    _sntprintf(_debugStr, 255, _T(__FILE__) _T(":") _T(STRING(__LINE__)) _T(" ") x _T("\n"), ##__VA_ARGS__), \
        OutputDebugString(_debugStr)
#else
#define DebugString
#endif
}
