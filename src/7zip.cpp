#include "7zip.h"
#include "contplug.h"
#include "cunicode.h"
#include <math.h>
#include <vector>
#include "7zip/Common/RegisterCodec.h"

#define REGISTER_CODEC_NAME(x) CRegisterCodec##x

#define MY_REGISTER_CODEC(x)            \
    struct REGISTER_CODEC_NAME(x)       \
    {                                   \
        REGISTER_CODEC_NAME(x)()        \
        {                               \
            RegisterCodec(&g_Codec##x); \
        }                               \
    };                                  \
    static REGISTER_CODEC_NAME(x) g_RegisterCodec##x;

constexpr static CCodecInfo g_CodecBZip2{ 0, 0, 0x040202, "BZip2", 1, false };

constexpr static CCodecInfo g_CodecBCJ2{ 0, 0, 0x0303011B, "BCJ2", 4, false };

constexpr static CCodecInfo g_CodecBCJ{ 0, 0, 0x03030103, "BCJ", 1, true };

constexpr static CCodecInfo g_CodecDeflate64{ 0, 0, 0x040109, "Deflate64", 1, false };

constexpr static CCodecInfo g_CodecDeflate{ 0, 0, 0x040108, "Deflate", 1, false };

constexpr static CCodecInfo g_CodecPPMD{ 0, 0, 0x030401, "PPMD", 1, false };

constexpr static CCodecInfo g_CodecLZMA2{0, 0, 0x21, "LZMA2", 1, false };

MY_REGISTER_CODEC(LZMA2)
MY_REGISTER_CODEC(PPMD)
MY_REGISTER_CODEC(Deflate)
MY_REGISTER_CODEC(Deflate64)
MY_REGISTER_CODEC(BCJ)
MY_REGISTER_CODEC(BCJ2)
MY_REGISTER_CODEC(BZip2)

constexpr static SupportedFieldDesc_t supportedFieds[] = {
    // must be the first suported fields (see ContentGetValue)
    { "Headers encrypted(true/false)", "", ft_boolean, HeadersEncryptedBooleanHandler },
    { "Headers encrypted(+/-)", "", ft_string, HeadersEncryptedStringHandler },

    { "Number of files", "", ft_numeric_32, NumberOfFilesHandler },
    { "Number of directories", "", ft_numeric_32, NumberOfDirsHandler },
    { "Total files", "", ft_numeric_32, TotalFilesHandler },
    { "Solid(true/false)", "", ft_boolean, SolidBooleanHandler },
    { "Solid(+/-)", "", ft_string, SolidStringHandler },
    { "Number of solid blocks", "", ft_numeric_32, NumberOfSolidBlocksHandler },
    { "Compress methods", "", ft_string, CompressMethodsHandler },
    { "Ratio %", "", ft_numeric_32, RatioHandler },
    { "Headers size", "", ft_numeric_64, HeadersSizeHandler },
    { "Uncompressed size(formatted string)", "", ft_string, UncompressedSizeFormattedHandler },
    { "Uncompressed size", "bytes|Kbytes|Mbytes|Gbytes", ft_numeric_64, UncompressedSizeHandler },
    { "Archive version", "", ft_string, VersionHandler }
};

constexpr static int NUM_SUPPORTED_FIELDS{ (sizeof(supportedFieds) / sizeof(supportedFieds[0])) };

struct ArchiveInfo_t
{
    wchar_t name[wdirtypemax]{};  // archive file name
    FILETIME MTime;               // archive file last modified time (used for caching)
    PluginArchiveInfo_t info;
};

constexpr static int ARCHIVE_CACHE_SIZE{ (NUM_SUPPORTED_FIELDS * 5) };
static ArchiveInfo_t* archiveCache{ nullptr };
static int archiveCacheIndex{ 0 };

static HINSTANCE g_hInstance{ nullptr };
constexpr static UInt64 gMaxCheckStartPosition{ 1 << 20 };

////////////////////////////////////////////////////////////////////////////////
// Unicode helper wrappers

#ifndef _UNICODE
bool g_IsNT{ false };
#endif

class C7zPassword : public ICryptoGetTextPassword, public CMyUnknownImp
{
public:
    MY_UNKNOWN_IMP

    bool passwordIsDefined;

    C7zPassword() : passwordIsDefined(false)
    {
    }

    ~C7zPassword()
    {
    }

    STDMETHOD(CryptoGetTextPassword)(BSTR* password)
    {
        passwordIsDefined = true;

        CMyComBSTR tempName;
        StringToBstr(tempName, password);

        return E_ABORT;
    }
};

////////////////////////////////////////////////////////////////////////////////
// Main DLL functions

BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD reason, LPVOID reserved)
{
    switch (reason) {
        case DLL_PROCESS_ATTACH:
            g_hInstance = hInst;
#ifndef _UNICODE
            g_IsNT = usys();
#endif
            break;

        case DLL_PROCESS_DETACH:
        default:
            break;
    }
    return TRUE;
}

static char* strlcpy(char* str1, const char* str2, int maxSize)
{
    if (int(std::strlen(str2)) >= maxSize - 1) {
        std::strncpy(str1, str2, maxSize - 1);
        str1[maxSize - 1] = '\0';
    } else {
        std::strcpy(str1, str2);
    }
    return str1;
}

static char* strlcat(char* str1, const char* str2, int maxSize)
{
    int len = (int)strlen(str1);
    if ((int)std::strlen(str2) >= maxSize - 1 - len) {
        std::strncat(str1, str2, maxSize - 1 - len);
        str1[maxSize - 1] = '\0';
    } else {
        std::strcat(str1, str2);
    }
    return str1;
}

void __stdcall ContentSetDefaultParams(ContentDefaultParamStruct* dps)
{
    archiveCache = (ArchiveInfo_t*)malloc(sizeof(ArchiveInfo_t) * ARCHIVE_CACHE_SIZE);
    ZeroMemory(archiveCache, sizeof(ArchiveInfo_t) * ARCHIVE_CACHE_SIZE);
    archiveCacheIndex = 0;
}

void __stdcall ContentPluginUnloading(void)
{
    if (archiveCache) {
        free(archiveCache);
        archiveCache = nullptr;
    }
}

int __stdcall ContentGetSupportedField(int FieldIndex, char* FieldName, char* Units, int maxlen)
{
    if (FieldIndex < 0 || FieldIndex >= NUM_SUPPORTED_FIELDS)
        return ft_nomorefields;
    else {
        strlcpy(FieldName, supportedFieds[FieldIndex].name, maxlen);
        strlcpy(Units, supportedFieds[FieldIndex].units, maxlen);
        return supportedFieds[FieldIndex].type;
    }
}

static void GetCompressMethods(NArchive::N7z::CDbEx& db, char* methodsOut)
{
    methodsOut[0] = '\0';

    AString methodName;
    for (unsigned i = 0; i < db.ParsedMethods.IDs.Size(); i++) {
        if (FindMethod(db.ParsedMethods.IDs[i], methodName)) {
            strlcat(methodsOut, methodName, 128);
        }
        if (i + 1 < db.ParsedMethods.IDs.Size())
            strlcat(methodsOut, " ", 128);
    }
}

static int ArchiveCache_FindOrAdd(const wchar_t* FileName)
{
    NWindows::NFile::NFind::CFileInfo fileInfo;

    if (!archiveCache)
        return -1;

    if (!fileInfo.Find(FileName) || fileInfo.IsDir())
        return -1;

    int i;
    for (i = 0; i < ARCHIVE_CACHE_SIZE; i++) {
        if (wcsicmp(FileName, archiveCache[i].name) == 0) {
            if (archiveCache[i].MTime.dwHighDateTime == fileInfo.MTime.dwHighDateTime &&
                archiveCache[i].MTime.dwLowDateTime == fileInfo.MTime.dwLowDateTime)
                return i;
            break;
        }
    }

    int retIndex = -1;

    CInFileStream* inStreamSpec = new CInFileStream;
    CMyComPtr<IInStream> inStream(inStreamSpec);
    if (inStreamSpec->OpenShared(FileName, false)) {
        NArchive::N7z::CInArchive archive(true);
        if (archive.Open(inStream, &gMaxCheckStartPosition) == S_OK) {
            NArchive::N7z::CDbEx db;
            C7zPassword myPass;
            bool _passwordIsDefined;
            bool _isEncrypted;
            UString _password;

            HRESULT hResult = archive.ReadDatabase(db, &myPass, _isEncrypted, _passwordIsDefined, _password);

            if (hResult == S_OK || (hResult == E_ABORT && myPass.passwordIsDefined)) {
                if (i == ARCHIVE_CACHE_SIZE) {
                    retIndex = archiveCacheIndex;
                    archiveCacheIndex++;
                    archiveCacheIndex %= ARCHIVE_CACHE_SIZE;
                } else
                    retIndex = i;

                ArchiveInfo_t& archInfo = archiveCache[retIndex];

                wcslcpy(archInfo.name, FileName, wdirtypemax);
                archInfo.MTime = fileInfo.MTime;

                if (hResult == S_OK) {
                    UInt64 totalUncompressedSize = 0;
                    int nFiles, totalFiles, nDirs;

                    totalFiles = db.Files.Size();
                    nFiles = nDirs = 0;
                    for (i = 0; i < totalFiles; i++) {
                        totalUncompressedSize += db.Files[i].Size;
                        if (db.Files[i].IsDir)
                            nDirs++;
                        else
                            nFiles++;
                    }

                    archInfo.info.Version = db.ArcInfo.Version;
                    archInfo.info.HeadersEncrypted = false;
                    archInfo.info.TotalFiles = totalFiles;
                    archInfo.info.NumDirs = nDirs;
                    archInfo.info.NumFiles = nFiles;
                    archInfo.info.HeadersSize = db.HeadersSize;
                    archInfo.info.IsSolid = db.IsSolid();
                    archInfo.info.NumSolidBlocks = db.NumFolders;
                    archInfo.info.UncompressedSize = totalUncompressedSize;
                    if (totalUncompressedSize)
                        archInfo.info.Ratio =
                            (int)(((db.PhySize - db.HeadersSize) / (float)totalUncompressedSize) * 100.f);
                    else
                        archInfo.info.Ratio = 0;

                    GetCompressMethods(db, archInfo.info.Methods);
                } else if (hResult == E_ABORT) {
                    archInfo.info.HeadersEncrypted = true;
                }
            }
        }
    }

    return retIndex;
}

int __stdcall ContentGetValue(const char* FileName, int FieldIndex, int UnitIndex, void* FieldValue, int maxSize,
                              int flags)
{
    std::vector<wchar_t> FileNameW;
    FileNameW.reserve(std::strlen(FileName) + 1);
    const int rc = ContentGetValueW(awlcopy(FileNameW.data(), FileName, int(FileNameW.capacity())), FieldIndex,
                                    UnitIndex, FieldValue, maxSize, flags);

    if (ft_numeric_floating == rc && maxSize > sizeof(double)) {
        wchar_t* p = _wcsdup((const wchar_t*)(static_cast<char*>(FieldValue) + sizeof(double)));
        walcopy(static_cast<char*>(FieldValue) + sizeof(double), p, maxSize - sizeof(double));
        free(p);
    }
    return rc;
}

int __stdcall ContentGetValueW(const wchar_t* FileName, int FieldIndex, int UnitIndex, void* FieldValue, int maxSize,
                               int flags)
{
    if (FieldIndex < 0 || FieldIndex >= NUM_SUPPORTED_FIELDS)
        return ft_nosuchfield;

    int cacheIndex = ArchiveCache_FindOrAdd(FileName);
    if (cacheIndex < 0)
        return ft_fileerror;

    if (archiveCache[cacheIndex].info.HeadersEncrypted && FieldIndex != 0 && FieldIndex != 1)
        return ft_fieldempty;

    int bytesWrote = supportedFieds[FieldIndex].handler(archiveCache[cacheIndex].info, UnitIndex, FieldValue, maxSize);
    if (bytesWrote <= 0)
        return ft_fieldempty;

    return supportedFieds[FieldIndex].type;
}

int __stdcall ContentGetDetectString(char* DetectString, int maxlen)
{
    /*sprintf_s (DetectString, maxlen, "EXT=\"7Z\"|([0]=%d & [1]=%d & [2]=%d & [3]=%d)", 
         (int)NArchive::N7z::kSignature[0], 
         (int)NArchive::N7z::kSignature[1], 
         (int)NArchive::N7z::kSignature[2], 
         (int)NArchive::N7z::kSignature[3]);*/
    strlcpy(DetectString, "EXT=\"7Z\"", maxlen);
    return 0;
}

//================================ Field Handlers ====================================
//====================================================================================

int HeadersEncryptedBooleanHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(int*)memOut = (int)archiveInfo.HeadersEncrypted;
    return sizeof(int);
}

int HeadersEncryptedStringHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    strlcpy((char*)memOut, (archiveInfo.HeadersEncrypted ? "+" : "-"), memSize);
    return 2;
}

int NumberOfFilesHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(int*)memOut = archiveInfo.NumFiles;
    return sizeof(int);
}

int NumberOfDirsHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(int*)memOut = archiveInfo.NumDirs;
    return sizeof(int);
}

int TotalFilesHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(int*)memOut = archiveInfo.TotalFiles;
    return sizeof(int);
}

int SolidBooleanHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(int*)memOut = (int)archiveInfo.IsSolid;
    return sizeof(int);
}

int SolidStringHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    strlcpy((char*)memOut, (archiveInfo.IsSolid ? "+" : "-"), memSize);
    return 2;
}

int NumberOfSolidBlocksHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(int*)memOut = archiveInfo.NumSolidBlocks;
    return sizeof(int);
}

int CompressMethodsHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    int len = (int)strlen(archiveInfo.Methods);
    if (memSize < len)
        return -1;
    strlcpy((char*)memOut, archiveInfo.Methods, memSize);
    return len;
}

int HeadersSizeHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(__int64*)memOut = (__int64)archiveInfo.HeadersSize;
    return sizeof(__int64);
}

int RatioHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    *(int*)memOut = archiveInfo.Ratio;
    return sizeof(int);
}

int UncompressedSizeHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    if (unitIndex < 0 || unitIndex > 3)
        return -1;
    *(__int64*)memOut = (__int64)archiveInfo.UncompressedSize >> (10 * unitIndex);
    return sizeof(__int64);
}

static inline float RoundTo(float val)
{
    return floorf(val * 10) / 10.f;
}

int UncompressedSizeFormattedHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    char buf[64];
    int len;

    if (archiveInfo.UncompressedSize < 1000)
        len = snprintf(buf, sizeof(buf), "%llu bytes", archiveInfo.UncompressedSize);
    else if (archiveInfo.UncompressedSize < 1000 * 1000)
        len = snprintf(buf, sizeof(buf), "%.1f Kb", RoundTo(archiveInfo.UncompressedSize / 1024.f));
    else if (archiveInfo.UncompressedSize < 1000 * 1000 * 1000)
        len = snprintf(buf, sizeof(buf), "%.1f Mb", RoundTo(archiveInfo.UncompressedSize / (float)(1024 * 1024)));
    else
        len =
            snprintf(buf, sizeof(buf), "%.1f Gb", RoundTo(archiveInfo.UncompressedSize / (float)(1024 * 1024 * 1024)));

    if (len < 0 || memSize < len)
        return -1;

    strlcpy((char*)memOut, buf, memSize);

    return len;
}

int VersionHandler(PluginArchiveInfo_t& archiveInfo, int unitIndex, void* memOut, int memSize)
{
    auto p{ static_cast<char*>(memOut) };
    int len = snprintf(p, memSize, "%x.%x", archiveInfo.Version.Major, archiveInfo.Version.Minor);

    if (len < 0 || memSize < len)
        return -1;

    return len;
}
