7zip content plugin v2.0.0.0 for Total Commander by Dmitry (Duny) Efimenko

SUPPORTED FIELDS:
====================
 - Headers encrypted flag
 - Headers size
 - Number of files
 - Number of directories
 - Total files (Number of files + Number of directories)
 - Uncompressed size
 - Ratio %
 - Solid flag
 - Number of solid blocks
 - Compression methods
 - Archive version
====================


NOTE:
====================
For archives with encrypted header only "Headers encrypted flag" is available.
====================


CONTACT:
====================
e-mail: tbbcmrjytv@gmail.com
russian forum: http://forum.wincmd.ru/viewtopic.php?p=46470
====================


HISTORY:
2.0.0.0: 04.02.2021
 - Switched to Visual Studio 2017
 - Also built 64-bit
 - Enabled Unicode support for file names
 - Enabled long path support for file names
 - Built with 7z version 19.00
 - Added field for archive version
 - Added .clang-format

0.2.3: 10.02.2009
 - optimized detect string
 - code optimizations and code reduction

0.2.2: 04.02.2009
 - rebuild with 7-zip 4.65 src
 - corrected "ratio" field calculation
 - LGPL

0.2.1: 03.02.2009
 - modified several fields
 - added "Number of solid blocks" field
 - added "Compression methods" field

0.1.1: 02.02.2009
 - Initial public release
